<?php
	require("../include/configuracao.inc.php");
?>
<table width="100%" border="0" cellspacing="2" cellpadding="0">
				<tr>
					<td colspan="7" class="subtitulo" height="26">&nbsp;<b>CONTAS REFERENTES</b></td>
				</tr>
				<tr>
					<td width="32" align="center" class="cantoLaranjaDestacado"><img src="imagens/apply.png" width="16" height="16"></td>
					<td align="center" class="cantoLaranjaDestacado">NOME</td>
					<td width="70" align="center" class="cantoLaranjaDestacado">PARCELA</td>
					<td width="100" align="center" class="cantoLaranjaDestacado">VENCIMENTO</td>
					<td width="80" align="center" class="cantoLaranjaDestacado">VALOR</td>
					<td width="32" align="center" class="cantoLaranjaDestacado"><img src="imagens/edit.png" width="16" height="16"></td>
					<td width="32" align="center" class="cantoLaranjaDestacado"><img src="imagens/stop.png" width="16" height="16"></td>
				</tr>
<?php 
	$sqlComando = "SELECT * FROM contas WHERE mesreferencia=$varMesCorrente AND anoreferencia=$varAnoCorrente ORDER BY datavencimento ASC";
	$sqlResultado = dbExecuta($dbConecta,$sqlComando);
	
	while ($sqlLinha = mysql_fetch_array($sqlResultado))
	{
		if (($sqlLinha["datavencimento"] < date(Y-m-d)) || ($sqlLinha["situacao"] == "npg")) { $vencido = "linhaAzulDestacado"; $valorNaoPago = $valorNaoPago + $sqlLinha["valor"]; } else { $vencido = "linhaAzul"; }
		if ($sqlLinha["situacao"] == "pg") { $pago = "disabled=\"disabled\""; $valorPago = $valorPago + $sqlLinha["valor"]; } else { $pago = ""; }
?>
				<tr>
					<td align="center"><input name="pagar" type="button" value="&radic;" class="cantoAzulDestacado" onClick="MM_goToURL('parent','?mes=<?php echo $varMesCorrente ?>&ano=<?php echo $varAnoCorrente ?>&pg=<?php echo $sqlLinha["id"]; ?>');return document.MM_returnValue" <?php echo $pago; ?> style="width:100%;" /></td>
					<td class="<?php echo $vencido ?>"><?php echo $sqlLinha["conta"]; ?></td>
					<td align="center" class="<?php echo $vencido ?>"><?php echo $sqlLinha["parcelaatual"] . "/" . $sqlLinha["parcelaqtd"]; ?></td>
					<td align="center" class="<?php echo $vencido ?>"><?php echo converteData($sqlLinha["datavencimento"],"normal"); ?></td>
					<td align="right" class="<?php echo $vencido ?>"><?php echo number_format($sqlLinha["valor"], 2, ',', '.'); ?></td>
					<td align="center"><input name="editar" type="button" value="+" class="cantoAzulDestacado" onClick="MM_goToURL('parent','?mes=<?php echo $varMesCorrente ?>&ano=<?php echo $varAnoCorrente ?>&editar=<?php echo $sqlLinha["id"]; ?>');return document.MM_returnValue" <?php echo $pago; ?> style="width:100%;" /></td>
					<td align="center"><input name="excluir" type="button" value="-" class="cantoAzulDestacado" onClick="MM_goToURL('parent','?mes=<?php echo $varMesCorrente ?>&ano=<?php echo $varAnoCorrente ?>&excluir=<?php echo $sqlLinha["id"]; ?>');return document.MM_returnValue" <?php echo $pago; ?> style="width:100%;" /></td>
				</tr>
<?php 
	$verificador = "existe";
	} 
	if ($verificador != "existe") {
		echo "<tr><td colspan=\"7\" align=\"center\" class=\"linhaLaranja\" height=\"26\">Nenhuma conta cadastrada referente a este m&ecirc;s!</td></tr>";
	} else {
		echo "<tr><td></td><td align=\"right\" class=\"linhaLaranjaDestacado\" colspan=\"3\"><b>TOTAL &Agrave; PAGAR:&nbsp;</b></td><td align=\"right\" class=\"cantoLaranjaDestacado\">". number_format($valorNaoPago, 2, ',', '.') ."</td></tr><tr><td></td><td align=\"right\" class=\"linhaLaranjaDestacado\" colspan=\"3\"><b>TOTAL PAGO:&nbsp;</b></td><td align=\"right\" class=\"cantoLaranjaDestacado\">". number_format($valorPago, 2, ',', '.') ."</td></tr>";
	}
?>
			</table>