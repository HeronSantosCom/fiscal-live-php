<?php 
	session_start();

	$dbHost = "localhost";
	$dbUsuario = "root";
	$dbSenha = "";
	$dbBanco = "fiscallive";
	
	if(!($dbConecta = mysql_connect($dbHost,$dbUsuario,$dbSenha))) {
		echo "N�o foi poss�vel estabelecer uma conex�o com o gerenciador MySQL. Favor Contactar o Administrador.";
		exit;
	} 
	if(!($dbSelect = mysql_select_db($dbBanco,$dbConecta))) { 
		echo "N�o foi poss�vel abrir o banco de dados no gerenciador MySQL. Favor Contactar o Administrador.";
		exit; 
	} 

	/*
		Esta fun��o executa um comando SQL no banco de dados MySQL
		$dbConexao - Ponteiro da Conex�o 
		$sqlComando - Cl�usula SQL a executar 
		$dbErro - Especifica se a fun��o exibe ou n�o(0=n�o, 1=sim) 
		$sqlResposta - Resposta
	*/ 
	function dbExecuta($dbConexao,$sqlComando,$dbErro = 1)
	{ 
		if(empty($sqlComando) OR !($dbConexao)) 
			return 0; //Erro na conex�o ou no comando SQL   
		if (!($sqlResposta = @mysql_query($sqlComando,$dbConexao)))
		{ 
			if($dbErro) 
				echo "Ocorreu um erro na execu��o do Comando SQL no banco de dados. Favor Contactar o Administrador.";
			exit;
		} 
		return $sqlResposta; 
	}
	
	function converteData($data,$formato)
	{
		if ($formato == "normal")
		{
			$dia = substr("$data", 8, 2);
			$mes = substr("$data", 5, 2);
			$ano = substr("$data", 0, 4);
			$dataConvertida = "$dia/$mes/$ano";
			return $dataConvertida;
		}
		if ($formato == "mysql")
		{
			$dia = substr("$data", 0, 2);
			$mes = substr("$data", 3, 2);
			$ano = substr("$data", 6, 4);
			$dataConvertida = "$ano-$mes-$dia";
			return $dataConvertida;
		}
	}	
	function converteMes($mes,$formato)
	{
		if ($formato == "extenso")
		{
			switch ($mes) {
				case "1": return "Janeiro";
				case "2": return "Fevereiro";
				case "3": return "Mar�o";
				case "4": return "Abril";
				case "5": return "Maio";
				case "6": return "Junho";
				case "7": return "Julho";
				case "8": return "Agosto";
				case "9": return "Setembro";
				case "10": return "Outubro";
				case "11": return "Novembro";
				case "12": return "Dezembro";
				default: return "???";
			}
		}
	}

	$varTitulo = "fisCal Live";
	$varVersao = "1.0";
	$varMesAtual = date(m);
	$varAnoAtual = date(Y);
	$varDataAtual = date(d) . "/" . date(m) . "/" . date(Y);
	
	// Verifica o mes atual
	$varMesSelecionado = $_GET["mes"];
	if (empty($varMesSelecionado))
	{
		$varMesCorrente = $varMesAtual;
	} else {
		$varMesCorrente = $varMesSelecionado;
	}
	
	$varAnoSelecionado = $_GET["ano"];
	if (empty($varAnoSelecionado))
	{
		$varAnoCorrente = $varAnoAtual;
	} else {
		$varAnoCorrente = $varAnoSelecionado;
	}
	
	$varEfetuaPagamento = $_GET["pg"];
	
	// Efetua pagamento
	if (!empty($varEfetuaPagamento))
	{
		$sql = mysql_query("SELECT COUNT(*) AS total FROM contas WHERE id=$varEfetuaPagamento AND situacao='pg'");
		$selectCount = mysql_result($sql, 0, "total");
		if ($selectCount == 0) {
			$sqlComando = "UPDATE contas SET situacao='pg' WHERE id=$varEfetuaPagamento";
			$sqlResultado = dbExecuta($dbConecta,$sqlComando);
		} else {
			$sqlComando = "UPDATE contas SET situacao='npg' WHERE id=$varEfetuaPagamento";
			$sqlResultado = dbExecuta($dbConecta,$sqlComando);
		}
	}

	if (($_GET["acao"] == "cadastrar") || ($_POST["acao"] == "Cadastrar"))
	{
		$mesreferencia		= $_POST["mesreferencia"]-1;
		$anoreferencia		= $_POST["anoreferencia"];
		$conta				= $_POST["conta"];
		$datainscricao		= date("Y-m-d");
		$datavencimento	= strtotime(converteData($_POST["datavencimento"],"mysql"));
		$valor				= $_POST["valor"];
		$parcelaatual		= $_POST["parcelaatual"]-1;
		$parcelaqtd			= $_POST["parcelaqtd"];
		$observacao			= $_POST["observacao"];
		for($i = 0; $i<$parcelaqtd; $i++)
		{
			$mesreferencia++;
			if ($mesreferencia > 12)
			{
				$mesreferencia = "01";
				$anoreferencia++;
			}
			$parcelaatual++;
			$datavencimentoparcelado = date("Y-m-d", strtotime("+$i month", $datavencimento));
			$sqlComando = "INSERT INTO contas SET
								id					= '',
								mesreferencia	= '$mesreferencia',
								anoreferencia	= '$anoreferencia',
								conta				= '$conta',
								datainscricao	= '$datainscricao',
								datavencimento	= '$datavencimentoparcelado',
								valor				= '$valor',
								parcelaatual	= '$parcelaatual',
								parcelaqtd		= '$parcelaqtd',
								observacao		= '$observacao'";
			$sqlResultado = dbExecuta($dbConecta,$sqlComando);
		}
	}
	if (!empty($_GET["excluir"]))
	{
		$id = $_GET["excluir"];
		$sqlComando = "DELETE FROM contas WHERE id = $id";
		$sqlResultado = dbExecuta($dbConecta,$sqlComando);
	}
?>