<?php
	require("include/configuracao.inc.php");
?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?php echo $varTitulo ?></title>
<style type="text/css">
<!--
.titulo {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 36px;
	font-weight: bolder;
	text-align: right;
	border-top-style: none;
	border-right-style: none;
	border-bottom-style: none;
	border-left-style: none;
}
.subtitulo {
	font-size: 14px;
	font-weight: bolder;
	border-top-width: 1px;
	border-right-width: 1px;
	border-bottom-width: 1px;
	border-left-width: 1px;
	border-top-style: none;
	border-right-style: none;
	border-bottom-style: none;
	border-left-style: none;
	font-family: Verdana, Arial, Helvetica, sans-serif;
}
.caixaLaranja {
	border-top-width: thick;
	border-right-width: thick;
	border-bottom-width: thick;
	border-left-width: thick;
	border-top-style: solid;
	border-right-style: solid;
	border-bottom-style: solid;
	border-left-style: solid;
	border-top-color: #999999;
	border-right-color: #E6E6E6;
	border-bottom-color: #E6E6E6;
	border-left-color: #999999;
}
.linhaAzul {
	border-top-width: 1px;
	border-right-width: 1px;
	border-bottom-width: 1px;
	border-left-width: 1px;
	border-top-style: none;
	border-right-style: none;
	border-bottom-style: solid;
	border-left-style: none;
	border-top-color: #336699;
	border-right-color: #336699;
	border-bottom-color: #336699;
	border-left-color: #336699;
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	background-color: #FFFFFF;
}
.linhaAzulDestacado {
	border-top-width: 1px;
	border-right-width: 1px;
	border-bottom-width: 1px;
	border-left-width: 1px;
	border-top-style: none;
	border-right-style: none;
	border-bottom-style: solid;
	border-left-style: none;
	border-top-color: #336699;
	border-right-color: #336699;
	border-bottom-color: #336699;
	border-left-color: #336699;
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	background-color: #E2ECF5;
}
.cantoAzul {
	border-top-width: 1px;
	border-right-width: 1px;
	border-bottom-width: 1px;
	border-left-width: 1px;
	border-top-style: none;
	border-right-style: solid;
	border-bottom-style: solid;
	border-left-style: none;
	border-top-color: #336699;
	border-right-color: #336699;
	border-bottom-color: #336699;
	border-left-color: #336699;
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	background-color: #FFFFFF;
}
.cantoAzulDestacado {
	border-top-width: 1px;
	border-right-width: 1px;
	border-bottom-width: 1px;
	border-left-width: 1px;
	border-top-style: none;
	border-right-style: solid;
	border-bottom-style: solid;
	border-left-style: none;
	border-top-color: #336699;
	border-right-color: #336699;
	border-bottom-color: #336699;
	border-left-color: #336699;
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	background-color: #E2ECF5;
}
.linhaLaranja {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	border-top-width: 1px;
	border-right-width: 1px;
	border-bottom-width: 1px;
	border-left-width: 1px;
	border-top-style: none;
	border-right-style: none;
	border-bottom-style: solid;
	border-left-style: none;
	border-top-color: #FF9900;
	border-right-color: #FF9900;
	border-bottom-color: #FF9900;
	border-left-color: #FF9900;
	background-color: #FFFFFF;
}
.linhaLaranjaDestacado {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	border-top-width: 1px;
	border-right-width: 1px;
	border-bottom-width: 1px;
	border-left-width: 1px;
	border-top-style: none;
	border-right-style: none;
	border-bottom-style: solid;
	border-left-style: none;
	border-top-color: #FF9900;
	border-right-color: #FF9900;
	border-bottom-color: #FF9900;
	border-left-color: #FF9900;
	background-color: #FFCF9C;
}
.cantoLaranja {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	border-top-width: 1px;
	border-right-width: 1px;
	border-bottom-width: 1px;
	border-left-width: 1px;
	border-top-style: none;
	border-right-style: solid;
	border-bottom-style: solid;
	border-left-style: none;
	border-top-color: #FF9900;
	border-right-color: #FF9900;
	border-bottom-color: #FF9900;
	border-left-color: #FF9900;
	background-color: #FFFFFF;
}
.cantoLaranjaDestacado {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	border-top-width: 1px;
	border-right-width: 1px;
	border-bottom-width: 1px;
	border-left-width: 1px;
	border-top-style: none;
	border-right-style: solid;
	border-bottom-style: solid;
	border-left-style: none;
	border-top-color: #FF9900;
	border-right-color: #FF9900;
	border-bottom-color: #FF9900;
	border-left-color: #FF9900;
	background-color: #FFCC99;
}
body {
	background-color: #CECFCE;
}
.caixaCinza {
	background-color: #CCCCCC;
	border-top-width: thick;
	border-right-width: thick;
	border-bottom-width: thick;
	border-left-width: thick;
	border-top-style: solid;
	border-right-style: solid;
	border-bottom-style: solid;
	border-left-style: solid;
	border-top-color: #E6E6E6;
	border-right-color: #999999;
	border-bottom-color: #999999;
	border-left-color: #E6E6E6;
}
-->
</style>
<script language="JavaScript" type="text/JavaScript"><!--
	function MM_goToURL() { //v3.0
		var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
		for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
	}
// ========================================================================
// -->> FORMATA EM MODO DE MOEDA
// ========================================================================
function limpaCaracter(valor, validos) {
	var result = "";
	var aux;
	for (var i=0; i < valor.length; i++) {
		aux = validos.indexOf(valor.substring(i, i+1));
		if (aux>=0) {
			result += aux;
		}
	}
	return result;
}
function formataCurrency(campo,tammax,teclapres,decimal) {
	var tecla = teclapres.keyCode;
	vr = limpaCaracter(campo.value,"0123456789");
	tam = vr.length;
	dec=decimal
	if (tam < tammax && tecla != 8){ tam = vr.length + 1 ; }
	if (tecla == 8 ) { tam = tam - 1; }
	if ( tecla == 8 || tecla >= 48 && tecla <= 57 || tecla >= 96 && tecla <= 105 ) {
		if ( tam <= dec ) { campo.value = vr ; }
		if ( (tam > dec) && (tam <= 5) ){ campo.value = vr.substr( 0, tam - 2 ) + "." + vr.substr( tam - dec, tam ); }
		if ( (tam >= 6) && (tam <= 8) ){ campo.value = vr.substr( 0, tam - 5 ) + "" + vr.substr( tam - 5, 3 ) + "." + vr.substr( tam - dec, tam ); }
		if ( (tam >= 9) && (tam <= 11) ){ campo.value = vr.substr( 0, tam - 8 ) + "" + vr.substr( tam - 8, 3 ) + "" + vr.substr( tam - 5, 3 ) + "." + vr.substr( tam - dec, tam ); }
		if ( (tam >= 12) && (tam <= 14) ){ campo.value = vr.substr( 0, tam - 11 ) + "" + vr.substr( tam - 11, 3 ) + "" + vr.substr( tam - 8, 3 ) + "" + vr.substr( tam - 5, 3 ) + "." + vr.substr( tam - dec, tam ); }
		if ( (tam >= 15) && (tam <= 17) ){ campo.value = vr.substr( 0, tam - 14 ) + "" + vr.substr( tam - 14, 3 ) + "" + vr.substr( tam - 11, 3 ) + "" + vr.substr( tam - 8, 3 ) + "" + vr.substr( tam - 5, 3 ) + "." + vr.substr( tam - 2, tam ); }
	} 
}

// ========================================================================
// -->> FORMATA OS CAMPOS(INPUT) DOS FORMUL�RIOS
// ========================================================================
function formataCampo(campo, mask, evt) { 
	if(document.all) { // Internet Explorer 
		key = evt.keyCode;
	} else { // Nestcape 
		key = evt.which; 
	} 
	string = campo.value;
	i = string.length;
	if (key != 08){
		if (i < mask.length) {
			if (mask.charAt(i) == '#') {
				return (key > 47 && key < 58);
			} else {
				if (mask.charAt(i) == '$') {
					return true;
				}
				for (c = i; c < mask.length; c++) {
					if (mask.charAt(c) != '#' && mask.charAt(c) != '$')
						campo.value = campo.value + mask.charAt(c);
					else if (mask.charAt(c) == '$') {
						return true;
					} else {
						return (key > 47 && key < 58);
					}
				}
			}
		} else
			return false;
	} else
			return (key);
}

// ========================================================================
// -->> BLOQUEIA AS LETRAS E ACENTOS, SOMENTE N�MEROS
// ========================================================================
function bloqueiaAlfa(evnt) {
	if (navigator.appName.indexOf('Microsoft') != -1) {
		if (evnt.keyCode == 42) {
			return (evnt.keyCode);
		} else {
			if (evnt.keyCode < 48 || evnt.keyCode > 57) { return false; }
		}
	} else {
		if (evnt.charCode == 42) {
			return (evnt.charCode);
		} else {
			if ((evnt.charCode < 48 || evnt.charCode > 57) && evnt.keyCode == 0) { return false; }
		}
	}
}
	
//--></script>
</head>
<body>
<table width="100%" border="0" cellpadding="0" cellspacing="10" bgcolor="#FFFFFF" class="caixaLaranja">
	<tr>
		<td height="50" align="center" valign="middle" class="caixaCinza"><span class="titulo"><?php echo $varTitulo ?></span></td>
	</tr>
	<tr>
		<td class="caixaCinza">
		<table width="100%" border="0" cellspacing="2" cellpadding="0">
			<tr>
				<td height="26" colspan="3">&nbsp;<span class="subtitulo">Calend&aacute;rio</span></td>
			</tr>
			<tr>
				<td><?php $anoanterior = $varAnoCorrente-1; ?>
					<input name="pagar33" type="button" value="<?php echo $anoanterior ?>" class="cantoLaranjaDestacado" onClick="MM_goToURL('parent','<?php echo "?mes=12&ano=$anoanterior" ?>');return document.MM_returnValue" style="width:100%;" /></td>
				<td colspan="12" align="center" class="cantoAzulDestacado"><?php echo $varAnoCorrente ?></td>
				<td><?php $anoposterior = $varAnoCorrente+1; ?>
					<input name="pagar34" type="button" value="<?php echo $anoposterior ?>" class="cantoLaranjaDestacado" onClick="MM_goToURL('parent','<?php echo "?mes=1&ano=$anoposterior" ?>');return document.MM_returnValue" style="width:100%;" /></td>
			</tr>
			<tr>
				<td><?php $mes = $varMesCorrente-1; if ($mes == "0") { $mes = 12; $ano = $varAnoCorrente-1; } else { $ano = $varAnoCorrente; } ?>
					<input name="pagar32" type="button" value="<<<" class="cantoLaranjaDestacado" onClick="MM_goToURL('parent','<?php echo "?mes=$mes&ano=$ano" ?>');return document.MM_returnValue" style="width:100%;" /></td>
				<?php 
	for ($xMes = 1; $xMes < 13; $xMes++)
	{
		if ($xMes == $varMesCorrente) { $mesatual = "cantoAzulDestacado"; } else { $mesatual = "cantoLaranjaDestacado"; }
				echo "<td><input name=\"pagar3\" type=\"button\" value=\"". converteMes($xMes,"extenso") ."\" class=\"".$mesatual."\" onClick=\"MM_goToURL('parent','?mes=".$xMes."&ano=".$varAnoCorrente."');return document.MM_returnValue\" style=\"width:100%;\" /></td>";
	}
?>
			<td><?php $mes = $varMesCorrente+1; if ($mes == "13") { $mes = 1; $ano = $varAnoCorrente+1; } else { $ano = $varAnoCorrente; } ?>
					<input name="pagar322" type="button" value=">>>" class="cantoLaranjaDestacado" onClick="MM_goToURL('parent','<?php echo "?mes=$mes&ano=$ano" ?>');return document.MM_returnValue" style="width:100%;" /></td>
			</tr>
		</table>
		</td>
	</tr>
	<tr>
		<td align="center" valign="top" class="caixaCinza"><table width="100%" border="0" cellspacing="2" cellpadding="0">
				<tr>
					<td height="26" colspan="7" class="subtitulo">&nbsp;Contas referentes � <?php echo converteMes($varMesCorrente,"extenso"). "/" . $varAnoCorrente; ?></td>
				</tr>
				<tr>
					<td width="32" align="center" class="cantoLaranjaDestacado"><img src="imagens/apply.png" width="16" height="16"></td>
					<td align="center" class="cantoLaranjaDestacado">NOME</td>
					<td width="70" align="center" class="cantoLaranjaDestacado">PARCELA</td>
					<td width="100" align="center" class="cantoLaranjaDestacado">VENCIMENTO</td>
					<td width="80" align="center" class="cantoLaranjaDestacado">VALOR</td>
					<td width="32" align="center" class="cantoLaranjaDestacado"><img src="imagens/stop.png" width="16" height="16"></td>
				</tr>
				<?php 
	$sqlComando = "SELECT * FROM contas WHERE mesreferencia=$varMesCorrente AND anoreferencia=$varAnoCorrente ORDER BY datavencimento ASC";
	$sqlResultado = dbExecuta($dbConecta,$sqlComando);
	
	while ($sqlLinha = mysql_fetch_array($sqlResultado))
	{
		if (($sqlLinha["datavencimento"] < date(Y-m-d)) || ($sqlLinha["situacao"] == "npg")) { $vencido = "linhaAzulDestacado"; $valorNaoPago = $valorNaoPago + $sqlLinha["valor"]; } else { $vencido = "linhaAzul"; }
		if ($sqlLinha["situacao"] == "pg") { $pago = "&radic;"; $valorPago = $valorPago + $sqlLinha["valor"]; } else { $pago = "&mdash;"; }
?>
				<tr>
					<td align="center"><input name="pagar" type="button" value="<?php echo $pago; ?>"  class="cantoAzulDestacado" onClick="MM_goToURL('parent','?mes=<?php echo $varMesCorrente ?>&ano=<?php echo $varAnoCorrente ?>&pg=<?php echo $sqlLinha["id"]; ?>');return document.MM_returnValue" style="width:100%;" /></td>
					<td class="<?php echo $vencido ?>"><?php echo $sqlLinha["conta"]; ?></td>
					<td align="center" class="<?php echo $vencido ?>"><?php echo $sqlLinha["parcelaatual"] . "/" . $sqlLinha["parcelaqtd"]; ?></td>
					<td align="center" class="<?php echo $vencido ?>"><?php echo converteData($sqlLinha["datavencimento"],"normal"); ?></td>
					<td align="right" class="<?php echo $vencido ?>"><?php echo number_format($sqlLinha["valor"], 2, ',', '.'); ?></td>
					<td align="center"><input name="excluir" type="button" value="-" class="cantoAzulDestacado" onClick="MM_goToURL('parent','?mes=<?php echo $varMesCorrente ?>&ano=<?php echo $varAnoCorrente ?>&excluir=<?php echo $sqlLinha["id"]; ?>');return document.MM_returnValue" <?php echo $pago; ?> style="width:100%;" /></td>
				</tr>
				<?php 
	$verificador = "existe";
	} 
	if ($verificador != "existe") {
		echo "<tr><td colspan=\"7\" align=\"center\" class=\"linhaLaranja\" height=\"26\">Nenhuma conta cadastrada referente a este m&ecirc;s!</td></tr>";
	} else {
		echo "<tr><td></td><td align=\"right\" class=\"linhaLaranjaDestacado\" colspan=\"3\"><b>TOTAL &Agrave; PAGAR:&nbsp;</b></td><td align=\"right\" class=\"cantoLaranjaDestacado\">". number_format($valorNaoPago, 2, ',', '.') ."</td></tr><tr><td></td><td align=\"right\" class=\"linhaLaranjaDestacado\" colspan=\"3\"><b>TOTAL PAGO:&nbsp;</b></td><td align=\"right\" class=\"cantoLaranjaDestacado\">". number_format($valorPago, 2, ',', '.') ."</td></tr>";
	}
?>
			</table></td>
	</tr>
	<tr>
		<td align="center" valign="top" class="caixaCinza"><table width="100%" border="0" cellspacing="2" cellpadding="0">
				<tr>
					<td height="26" colspan="5" class="subtitulo">&nbsp;Contas pendentes anteriores � <?php echo converteMes($varMesCorrente,"extenso"). "/" . $varAnoCorrente; ?></td>
				</tr>
				<tr>
					<td width="32" align="center" class="cantoLaranjaDestacado"><img src="imagens/apply.png" width="16" height="16"></td>
					<td width="70%" align="center" class="cantoLaranjaDestacado">NOME</td>
					<td width="70" align="center" class="cantoLaranjaDestacado">PARCELA</td>
					<td width="100" align="center" class="cantoLaranjaDestacado">VENCIMENTO</td>
					<td width="80" align="center" class="cantoLaranjaDestacado">VALOR</td>
				</tr>
				<?php 
	$sqlComando = "SELECT * FROM contas WHERE (mesreferencia<$varMesCorrente) OR (anoreferencia<$varAnoCorrente) AND (situacao='npg') ORDER BY datavencimento ASC";
	$sqlResultado = dbExecuta($dbConecta,$sqlComando);
	
	while ($sqlLinha = mysql_fetch_array($sqlResultado))
	{
		if (($sqlLinha["situacao"] == "npg"))
		{
			if ($sqlLinha["anoreferencia"] <= $varAnoCorrente)
			{
				$valorTotal = $valorTotal + $sqlLinha["valor"];
?>
				<tr>
					<td align="center"><input name="pagar2" type="button" value="&radic;" class="cantoAzulDestacado" onClick="MM_goToURL('parent','?mes=<?php echo $varMesCorrente ?>&ano=<?php echo $varAnoCorrente ?>&pg=<?php echo $sqlLinha["id"]; ?>');return document.MM_returnValue" <?php echo $pago; ?> style="width:100%;" /></td>
					<td class="linhaAzul"><?php echo $sqlLinha["conta"]; ?></td>
					<td align="center" class="linhaAzul"><?php echo $sqlLinha["parcelaatual"] . "/" . $sqlLinha["parcelaqtd"]; ?></td>
					<td align="center" class="linhaAzul"><?php echo converteData($sqlLinha["datavencimento"],"normal"); ?></td>
					<td align="right" class="cantoAzul"><?php echo number_format($sqlLinha["valor"], 2, ',', '.'); ?></td>
				</tr>
				<?php 
				$verificadorPendente = "existe";
			}
		}
	}

	if ($verificadorPendente != "existe") {
		echo "<tr><td colspan=\"7\" align=\"center\" class=\"linhaLaranja\" height=\"26\">Nenhuma conta pendentes anterior a este m&ecirc;s!</td></tr>";
	} else {
		echo "<tr><td></td><td align=\"right\" class=\"linhaLaranjaDestacado\" colspan=\"3\"><b>TOTAL &Agrave; PAGAR:&nbsp;</b></td><td align=\"right\" class=\"cantoLaranjaDestacado\">". number_format($valorTotal, 2, ',', '.') ."</td></tr>";
	}
?>
			</table></td>
	</tr>
	<tr>
		<td align="center" valign="top" class="caixaCinza"><table width="100%" border="0" cellspacing="2" cellpadding="0">
				<tr>
					<td height="26" class="subtitulo">&nbsp;Cadastro de contas </td>
				</tr>
				<tr>
					<td><form action="?acao=cadastrar" method="post" name="cadastrar" id="cadastrar">
							<table width="100%" border="0" align="center" cellpadding="0" cellspacing="2">
								<tr>
									<td width="150" class="linhaAzulDestacado">M&ecirc;s de refer&ecirc;ncia:</td>
									<td><select name="mesreferencia" id="mesreferencia" class="cantoLaranjaDestacado" style="width:25%">
											<?php 
	for ($mes=1;$mes<13;$mes++) {
		if ($mes == $varMesCorrente) { $mesatual = "selected=\"selected\""; } else { $mesatual = ""; }
			echo "<option value=\"".$mes."\" $mesatual>".converteMes($mes,"extenso")."</option>";
	}
?>
										</select>
										<select name="anoreferencia" id="anoreferencia" class="cantoLaranjaDestacado" style="width:10%">
											<?php 
	for ($ano=2000;$ano<2010;$ano++) {
		if ($ano == $varAnoCorrente) { $anoatual = "selected=\"selected\""; } else { $anoatual = ""; }
			echo "<option value=\"".$ano."\" $anoatual>".$ano."</option>";
	}
?>
										</select>
									</td>
								</tr>
								<tr>
									<td class="linhaAzulDestacado">Nome da conta:</td>
									<td ><input name="conta" type="text" class="cantoLaranjaDestacado" id="conta" size="70" maxlength="150" style="width:100%" onKeyUp="this.value = this.value.toUpperCase();"></td>
								</tr>
								<tr>
									<td class="linhaAzulDestacado">Data do vencimento:</td>
									<td ><input name="datavencimento" type="text" class="cantoLaranjaDestacado" id="datavencimento" size="25" maxlength="10" style="width:35%" onKeyPress="formataCampo(this, '##/##/####', event);return bloqueiaAlfa(event);"></td>
								</tr>
								<tr>
									<td class="linhaAzulDestacado">Valor: </td>
									<td ><input name="valor" type="text" class="cantoLaranjaDestacado" id="valor" size="25" maxlength="13" style="width:20%; text-align:right" onKeyDown="formataCurrency(this,10,event,2)" onKeyPress="return bloqueiaAlfa(event);"></td>
								</tr>
								<tr>
									<td class="linhaAzulDestacado">Quantidade de parcelas:</td>
									<td ><select name="parcelaatual" id="parcelaatual" class="cantoLaranjaDestacado" style="width:10%">
											<?php 
	for ($patual=1;$patual<32;$patual++) {
		if ($patual == "1") { $ppatualatual = "selected=\"selected\""; } else { $ppatualatual = ""; }
			echo "<option value=\"".$patual."\" $ppatualatual>".$patual."</option>";
	}
?>
										</select>
										<select name="parcelaqtd" id="parcelaqtd" class="cantoLaranjaDestacado" style="width:10%">
											<?php 
	for ($pqtd=1;$pqtd<32;$pqtd++) {
		if ($pqtd == "1") { $pqtdatual = "selected=\"selected\""; } else { $pqtdatual = ""; }
			echo "<option value=\"".$pqtd."\" $pqtdatual>".$pqtd."</option>";
	}
?>
										</select></td>
								</tr>
								<tr>
									<td class="linhaAzulDestacado">Observa&ccedil;&atilde;o: </td>
									<td><input name="observacao" type="text" class="linhaLaranjaDestacado" id="observacao" size="80" maxlength="255" style="width:100%" onKeyUp="this.value = this.value.toUpperCase();"></td>
								</tr>
							</table>
							<table width="80%" border="0" align="center" cellpadding="0" cellspacing="10">
								<tr>
									<td width="50%" align="center"><input name="acao" type="submit" class="cantoAzulDestacado" id="acao" value="Cadastrar" style="width:100%"></td>
									<td width="50%" align="center"><input name="acao" type="reset" class="cantoAzulDestacado" id="acao" value="Limpar" style="width:100%"></td>
								</tr>
							</table>
						</form></td>
				</tr>
			</table></td>
	</tr>
	<tr>
		<td colspan="2" align="center" valign="top" class="caixaCinza">&nbsp;</td>
	</tr>
</table>
</body>
</html>
